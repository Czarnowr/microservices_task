package com.microservices.task.controller;

import com.microservices.task.model.Task;
import com.microservices.task.model.dto.TaskDto;
import com.microservices.task.service.RestTaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/api/task")
public class TaskController {

    private RestTaskService restTaskService;

    @Autowired
    public TaskController(final RestTaskService restTaskService) {
        this.restTaskService = restTaskService;
    }

    @PutMapping(path = "/add")
    @ResponseStatus(value = HttpStatus.CREATED)
    private Long createTask(@RequestBody TaskDto taskDto) {
        return restTaskService.create(taskDto);
    }

    @GetMapping("{taskId}")
    @ResponseStatus(HttpStatus.OK)
    private Task getTask(Long taskId) {
        return restTaskService.get(taskId);
    }

    @GetMapping("{ownerId}")
    @ResponseStatus(HttpStatus.OK)
    private List<Task> listTasksByOwner(Long ownerId) {
        return restTaskService.listTasksByOwnerId(ownerId);
    }

    @PostMapping("{taskId}")
    @ResponseStatus(HttpStatus.OK)
    private TaskDto updateTask(@RequestBody TaskDto taskDto,
                               Long taskId) {
        return restTaskService.update(taskId, taskDto);
    }

    @DeleteMapping("{taskId}")
    @ResponseStatus(HttpStatus.OK)
    private void deleteTask(Long taskId) {
        restTaskService.delete(taskId);
    }
}
