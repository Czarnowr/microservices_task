package com.microservices.task.service;

import com.microservices.task.model.Task;
import com.microservices.task.model.dto.TaskDto;
import com.microservices.task.repository.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;

@Service
public class RestTaskService {

    private TaskRepository taskRepository;

    @Autowired
    public RestTaskService(final TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public Long create(TaskDto taskDto) {
        Task task = taskRepository.save(mapFromTaskDto(new Task(), taskDto));

        return task.getId();
    }

    public Task get(final Long id) {
        return getTaskFromDatabase(id);
    }

    public TaskDto update(final Long id, final TaskDto taskDto) {
        Task task = mapFromTaskDto(getTaskFromDatabase(id), taskDto);

        task = taskRepository.save(task);

        return mapFromTask(taskDto, task);
    }

    public void delete(final Long id){
        taskRepository.deleteById(id);
    }

    private Task getTaskFromDatabase(Long id){
        Optional<Task> userOptional = taskRepository.findById(id);

        if (!userOptional.isPresent()) {
            throw new EntityNotFoundException("User not found");
        }

        return userOptional.get();
    }

    private TaskDto mapFromTask(TaskDto taskDto, Task task) {
        taskDto.setOwnerId(task.getOwnerId());
        taskDto.setDescription(task.getDescription());
        return taskDto;
    }

    private Task mapFromTaskDto(Task task, TaskDto taskDto) {
        task.setOwnerId(taskDto.getOwnerId());
        task.setDescription(taskDto.getDescription());
        return task;
    }


    public List<Task> listTasksByOwnerId(final Long id) {
        return taskRepository.findAllByOwnerId(id);
    }
}
